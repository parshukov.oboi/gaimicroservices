create sequence fine_sequence;

CREATE TABLE IF NOT EXISTS fine
(
    id    INT8 PRIMARY KEY NOT NULL DEFAULT nextval('fine_sequence'),
    name  VARCHAR(500),
    price DOUBLE PRECISION
);

INSERT INTO fine ( name, price)
VALUES ( 'Управление ТС, не зарегистрированным в установленном порядке', 500),
       ( 'Установка на ТС заведомо подложных государственных регистрационных знаков', 7500),
       ( 'Проезд на красный', 750),
       ( 'Объезд пробки по обочине', 6000),
       ( 'Превышение скорости', 500);
--
--
-- @Getter
-- @Setter
-- @AllArgsConstructor
-- @Entity
-- @Table(name = "fine")
-- @NoArgsConstructor
-- @ToString
-- public class FineEntity {
--
--     @Id
--     @SequenceGenerator(name = "fineSequence", sequenceName = "fine_sequence", allocationSize = 1)
--     @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "fineSequence")
--     @Column(name = "id")
--     private Long id;
--
-- @Column(name = "name")
--     private String name;
--
-- @Column(name = "price")
--     private Double price;
-- }
