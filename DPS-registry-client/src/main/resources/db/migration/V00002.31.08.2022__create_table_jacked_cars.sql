create sequence jacked_cars_sequence;

CREATE TABLE IF NOT EXISTS jacked_cars
(
    id    INT8 PRIMARY KEY NOT NULL DEFAULT nextval('jacked_cars_sequence'),
    gos_number VARCHAR (20),
    expiration_of_hijacking timestamp,
    date_found timestamp
);

INSERT INTO jacked_cars (gos_number, expiration_of_hijacking,date_found)
VALUES ('A120XM','1994-04-30 00:00:00', '1994-12-30 00:00:00'),
       ('A230XM', '1995-08-30 00:00:00',null);





