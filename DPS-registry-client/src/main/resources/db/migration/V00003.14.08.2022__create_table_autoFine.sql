create sequence auto_sequence;

CREATE TABLE IF NOT EXISTS auto
(
    id    INT8 PRIMARY KEY NOT NULL DEFAULT nextval('auto_sequence'),
    fine_entities_id INT8 REFERENCES fine(id),
    gos_number VARCHAR (20)
);

INSERT INTO auto (fine_entities_id,gos_number)
VALUES (2,'A120XM'),
        (4,'A120XM'),
        (4,'A734XM'),
        (4,'A734XM');




