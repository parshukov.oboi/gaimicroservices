package org.gai.micro.dpsregistryclient;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableEurekaClient
public class DpsRegistryClientApplication {

	public static void main(String[] args) {
		SpringApplication.run(DpsRegistryClientApplication.class, args);
	}

}
