package org.gai.micro.dpsregistryclient.service;

import org.gai.micro.dpsregistryclient.entity.JackedCarsEntity;

public interface GetAutoJackedService {

    public Boolean getJackedCar(String gosNumber);
}
