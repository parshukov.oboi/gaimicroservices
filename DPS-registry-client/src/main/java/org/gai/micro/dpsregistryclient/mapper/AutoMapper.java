package org.gai.micro.dpsregistryclient.mapper;

import org.gai.micro.dpsregistryclient.dto.AutoFineDto;
import org.gai.micro.dpsregistryclient.entity.AutoEntity;
import org.springframework.stereotype.Component;

@Component
public class AutoMapper {

    public AutoFineDto getMapper(AutoEntity autoEntity){

        AutoFineDto autoFineDto = new AutoFineDto();
        autoFineDto.setGosNumber(autoEntity.getGosNumber());
        autoFineDto.setName(autoEntity.getFineEntities().getName());
        autoFineDto.setPrice(autoEntity.getFineEntities().getPrice());
        return  autoFineDto;

    }
}
