package org.gai.micro.dpsregistryclient.entity;

import lombok.*;

import javax.persistence.*;
import java.sql.Date;
import java.time.LocalDateTime;

@Getter
@Setter
@AllArgsConstructor
@Entity
@Table(name = "jacked_cars")
@NoArgsConstructor
@ToString
public class JackedCarsEntity {

    @Id
    @SequenceGenerator(name = "jackedCarsSequence", sequenceName = "jackedCars_sequence", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "jackedCarsSequence")
    @Column(name = "id")
    private Long id;


    @Column(name = "gos_number")
    private String gosNumber;

    @Column(name = "expiration_of_hijacking")
    private LocalDateTime dateOfHijacking;

    @Column(name = "date_found")
    private LocalDateTime dateFound;
}
