package org.gai.micro.dpsregistryclient.repository;

import org.gai.micro.dpsregistryclient.entity.FineEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FineRepository extends JpaRepository<FineEntity,Long> {
}
