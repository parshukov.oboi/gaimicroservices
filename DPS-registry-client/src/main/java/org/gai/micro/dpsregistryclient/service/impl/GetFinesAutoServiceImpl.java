package org.gai.micro.dpsregistryclient.service.impl;

import org.gai.micro.dpsregistryclient.dto.AutoFineDto;
import org.gai.micro.dpsregistryclient.entity.AutoEntity;
import org.gai.micro.dpsregistryclient.mapper.AutoMapper;
import org.gai.micro.dpsregistryclient.repository.AutoFineRepository;
import org.gai.micro.dpsregistryclient.service.GetFinesAutoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class GetFinesAutoServiceImpl implements GetFinesAutoService {

    @Autowired
    private AutoFineRepository autoFineRepository;

    @Autowired
    private AutoMapper autoMapper;

    @Override
    public List<AutoFineDto> getAuto(String gosNumber) {
        List<AutoEntity> autoEntities = autoFineRepository.autoInfo(gosNumber);
        List < AutoFineDto> autoFineDto = new ArrayList<>();
        for (AutoEntity autoEntity:autoEntities) {
            AutoFineDto mapper = autoMapper.getMapper(autoEntity);
            autoFineDto.add(mapper);
        }
        return autoFineDto;
    }

    @Override
    public Boolean getAutoMro(String gosNumber) {
        if (getAuto(gosNumber).size()==0) return true;
        else System.out.println("Есть неоплаченные штрафы");
        return false;
    }
}
