package org.gai.micro.dpsregistryclient.repository;

import org.gai.micro.dpsregistryclient.entity.JackedCarsEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface JackedRepository extends JpaRepository <JackedCarsEntity,Long> {

    public JackedCarsEntity getJackedCarsEntityByGosNumber(String gosNumber);
}
