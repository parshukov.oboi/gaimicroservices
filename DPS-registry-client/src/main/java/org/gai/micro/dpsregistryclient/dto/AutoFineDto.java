package org.gai.micro.dpsregistryclient.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;


@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class AutoFineDto {

    private String gosNumber;

    private String name;

    private Double price;
}
