package org.gai.micro.dpsregistryclient.entity;

import lombok.*;

import javax.persistence.*;

@Getter
@Setter
@AllArgsConstructor
@Entity
@Table(name = "auto")
@NoArgsConstructor
@ToString
public class AutoEntity {

    @Id
    @SequenceGenerator(name = "autoSequence", sequenceName = "auto_sequence", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "autoSequence")
    @Column(name = "id")
    private Long id;

    @ManyToOne
    private FineEntity fineEntities;

    @Column(name = "gos_number")
    private String gosNumber;



}
