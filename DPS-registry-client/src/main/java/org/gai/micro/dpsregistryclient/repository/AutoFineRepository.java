package org.gai.micro.dpsregistryclient.repository;

import org.gai.micro.dpsregistryclient.dto.AutoFineDto;
import org.gai.micro.dpsregistryclient.entity.AutoEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AutoFineRepository extends JpaRepository <AutoEntity,Long> {

        List<AutoEntity> getAutoEntitiesByGosNumber(String gosNumber);

        @Query(nativeQuery = true,value = "SELECT * FROM dps_registry_db.public.auto JOIN fine on auto.fine_entities_id = fine.id where gos_number =:gosNumber")
        List<AutoEntity> autoInfo(String gosNumber);
}
