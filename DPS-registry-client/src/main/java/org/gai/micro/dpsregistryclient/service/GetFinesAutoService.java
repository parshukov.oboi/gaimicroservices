package org.gai.micro.dpsregistryclient.service;

import org.gai.micro.dpsregistryclient.dto.AutoFineDto;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;


public interface GetFinesAutoService {
    List<AutoFineDto> getAuto ( String gosNumber);

    Boolean getAutoMro (String gosNumber);
}
