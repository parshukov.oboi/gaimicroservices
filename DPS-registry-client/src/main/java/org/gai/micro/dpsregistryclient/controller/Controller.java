package org.gai.micro.dpsregistryclient.controller;

import org.gai.micro.dpsregistryclient.dto.AutoFineDto;
import org.gai.micro.dpsregistryclient.entity.AutoEntity;
import org.gai.micro.dpsregistryclient.mapper.AutoMapper;
import org.gai.micro.dpsregistryclient.repository.AutoFineRepository;
import org.gai.micro.dpsregistryclient.service.GetAutoJackedService;
import org.gai.micro.dpsregistryclient.service.GetFinesAutoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
public class Controller {

    @Autowired
    private GetAutoJackedService getAutoJackedService;

    @Autowired
    private GetFinesAutoService getFinesAutoService;

    @GetMapping("/get-jacked")
    public Boolean jackedTest(String gosNumber) {
        return getAutoJackedService.getJackedCar(gosNumber);
    }

    @GetMapping("/get-fine")
    public @ResponseBody List<AutoFineDto> getAuto(@RequestParam String gosNumber) {
        return  getFinesAutoService.getAuto(gosNumber);
    }

    @GetMapping("/get-fine-for-mro")
    public Boolean getAutoMro(@RequestParam String gosNumber) {
        return  getFinesAutoService.getAutoMro(gosNumber);
    }


}
