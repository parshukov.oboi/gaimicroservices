package org.gai.micro.dpsregistryclient.service.impl;

import org.gai.micro.dpsregistryclient.entity.JackedCarsEntity;
import org.gai.micro.dpsregistryclient.repository.JackedRepository;
import org.gai.micro.dpsregistryclient.service.GetAutoJackedService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class GetAutoJackedServiceImpl implements GetAutoJackedService {

    @Autowired
    JackedRepository jackedRepository;

    @Override
    public Boolean getJackedCar(String gosNumber) {
        JackedCarsEntity jackedCarsEntityByGosNumber = jackedRepository.getJackedCarsEntityByGosNumber(gosNumber);
        if (jackedCarsEntityByGosNumber == null) {
            System.out.println("Машина в угоне не числится");
            return true;
        } else if (jackedCarsEntityByGosNumber.getDateFound() == null) {
            System.out.println("Машина чиcлится в угоне");
            return false;
        } else {
            System.out.println("Машина числилась в угоне, но сейчас уже найдена");
            return true;
        }
    }
}
