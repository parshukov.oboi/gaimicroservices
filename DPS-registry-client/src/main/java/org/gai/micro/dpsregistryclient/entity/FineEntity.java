package org.gai.micro.dpsregistryclient.entity;

import lombok.*;

import javax.persistence.*;

@Getter
@Setter
@AllArgsConstructor
@Entity
@Table(name = "fine")
@NoArgsConstructor
@ToString
public class FineEntity {

    @Id
    @SequenceGenerator(name = "fineSequence", sequenceName = "fine_sequence", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "fineSequence")
    @Column(name = "id")
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "price")
    private Double price;
}
