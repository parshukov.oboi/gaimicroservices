package org.gai.micro.insuranceregistryclient.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.time.LocalDateTime;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class InsuranceDto {

    private String vinCode;

    private LocalDateTime startDate;

    private LocalDateTime expirationDate;
}
