package org.gai.micro.insuranceregistryclient.entity;

import lombok.*;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;

@Getter
@Setter
@AllArgsConstructor
@Data
@Entity(name = "insurance")
@NoArgsConstructor
@ToString
public class InsuranceRegistryEntity {

    @Id
    @SequenceGenerator(name = "insuranceSequence", sequenceName = "insurance_sequence", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "insuranceSequence")
    @Column(name = "id")
    private Long id;

    @Column(name = "vin_code")
    private String vinCode;

    @Column(name = "start_date")
    private LocalDateTime startDate;

    @Column(name = "expiration_date")
    private LocalDateTime expirationDate;



}
