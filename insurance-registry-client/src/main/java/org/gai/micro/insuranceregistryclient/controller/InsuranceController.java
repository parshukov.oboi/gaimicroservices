package org.gai.micro.insuranceregistryclient.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import org.gai.micro.insuranceregistryclient.dto.InsuranceDto;
import org.gai.micro.insuranceregistryclient.entity.InsuranceRegistryEntity;
import org.gai.micro.insuranceregistryclient.repository.InsuranceRegistryRepository;
import org.gai.micro.insuranceregistryclient.repository.InsuranceRegistryRepositoryVin;
import org.gai.micro.insuranceregistryclient.service.impl.GetInsuranceDtoImpl;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class InsuranceController {

    @Autowired
    private GetInsuranceDtoImpl getInsuranceDto;

    @Autowired
    private InsuranceRegistryRepository insuranceRegistryRepository;


    @GetMapping("/get-insurance-info")
    public @ResponseBody InsuranceDto getInsuranceInfo(@RequestParam String vinCode){
        return  getInsuranceDto.getInsuranceInfo(vinCode);

    }

    @PostMapping("/add-insurance")
    public  void addInsurance(
            @RequestBody InsuranceRegistryEntity insuranceRegistryEntity
    ){
        insuranceRegistryRepository.save(insuranceRegistryEntity);

    }
}
