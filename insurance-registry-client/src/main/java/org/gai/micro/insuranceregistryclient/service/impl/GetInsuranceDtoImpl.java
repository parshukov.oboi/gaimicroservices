package org.gai.micro.insuranceregistryclient.service.impl;

import org.gai.micro.insuranceregistryclient.dto.InsuranceDto;
import org.gai.micro.insuranceregistryclient.entity.InsuranceRegistryEntity;
import org.gai.micro.insuranceregistryclient.repository.InsuranceRegistryRepositoryVin;
import org.gai.micro.insuranceregistryclient.service.GetInsuranceDtoService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class GetInsuranceDtoImpl implements GetInsuranceDtoService {

    @Autowired
    private InsuranceRegistryRepositoryVin insuranceRegistryRepositoryVin;

    @Autowired
    private ModelMapper modelMapper;

    @Override
    public InsuranceDto getInsuranceInfo(String vin) {

        List<InsuranceRegistryEntity> registryEntityList =
                insuranceRegistryRepositoryVin.findElementsByVinCode(vin);

        for (InsuranceRegistryEntity insuranceEntity:registryEntityList) {
            if (insuranceEntity.getExpirationDate()!=null ){
                return modelMapper.map(insuranceEntity, InsuranceDto.class);
            }
        }
        return null;
    }
}
