package org.gai.micro.insuranceregistryclient.repository;

import lombok.NoArgsConstructor;
import org.gai.micro.insuranceregistryclient.entity.InsuranceRegistryEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface InsuranceRegistryRepositoryVin extends JpaRepository<InsuranceRegistryEntity,Long> {

    @Query(nativeQuery = true,value = "SELECT * FROM insurence_registry WHERE vin_code =:vinCode")
    List<InsuranceRegistryEntity> findElementsByVinCode(String vinCode);
}
