package org.gai.micro.insuranceregistryclient.repository;

import org.gai.micro.insuranceregistryclient.entity.InsuranceRegistryEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface InsuranceRegistryRepository extends JpaRepository <InsuranceRegistryEntity,Long> {
}
