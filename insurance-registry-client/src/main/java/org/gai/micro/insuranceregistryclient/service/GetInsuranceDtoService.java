package org.gai.micro.insuranceregistryclient.service;


import org.gai.micro.insuranceregistryclient.dto.InsuranceDto;
import org.springframework.web.bind.annotation.ResponseBody;

public interface GetInsuranceDtoService {

     InsuranceDto getInsuranceInfo (String vin);

}
