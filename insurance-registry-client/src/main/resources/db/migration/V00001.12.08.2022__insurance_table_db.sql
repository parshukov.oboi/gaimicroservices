-- CREATE TABLE IF NOT EXISTS insurance
-- (
--     id    int8 PRIMARY KEY NOT NULL,
--     vin_code VARCHAR (100),
--     start_date timestamp,
--     expiration_date timestamp
-- );
-- create sequence insurance_sequence;
create sequence insurance_sequence;

create table insurance
(
    id    int8 not null default nextval('insurance_sequence')
        primary key,
    expiration_date timestamp,
    start_date      timestamp,
    vin_code        varchar(255)
);

INSERT INTO insurance ( vin_code, start_date, expiration_date)
VALUES ( 'WAUZZZ44ZEN096063', '2017-06-30 00:00:00', '2020-06-30 00:00:00'),
       ( 'WEEZZZ44ZEN092345', '2011-06-30 00:00:00', '2022-06-30 00:00:00'),
       ( 'QWEZZZ44ZEN096789', '2012-06-30 00:00:00', '2023-06-30 00:00:00'),
       ( 'WASDFZ44ZEN096543', '2013-06-30 00:00:00', '2034-06-30 00:00:00'),
       ( 'WVBNMZ44ZEN093463', '2014-06-30 00:00:00', '2019-06-30 00:00:00'),
       ( 'WQWEZZ44ZEN094363', '2015-06-30 00:00:00', '2005-06-30 00:00:00');


-- vin_code,start_date,expiration_date
