package com.gai.micro.mrogibddclient.service.impl;


import com.gai.micro.mrogibddclient.dto.forCar.InsuranceDto;
import com.gai.micro.mrogibddclient.feign.FeignClientInsurance;
import com.gai.micro.mrogibddclient.service.InsuranceService;
import com.gai.micro.mrogibddclient.service.MroGibddService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class InsuranceServiceImpl implements InsuranceService {

    @Autowired
    private MroGibddService mroGibddService;

    @Autowired
    private FeignClientInsurance feignClientInsurance;

    @Override
    public Boolean getInsurance(String vin) {
        InsuranceDto resultDto = feignClientInsurance.getInsuranceInfo(vin);
        if (resultDto==null){
            System.out.println("Даннные не найдены");
            return false;
        }
        else if (!mroGibddService.dateCheck(resultDto)) {
            System.out.println("В регистрации отказано, автомобиль имеет просроченую страховку");
            return false ;
        }else {
            System.out.println("Проверка строховки завершена. Результат: успешно");
            return true;
        }
    }
}
