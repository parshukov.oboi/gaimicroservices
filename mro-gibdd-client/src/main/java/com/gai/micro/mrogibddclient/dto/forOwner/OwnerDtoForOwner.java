package com.gai.micro.mrogibddclient.dto.forOwner;

import com.gai.micro.mrogibddclient.entity.AutoEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.time.LocalDateTime;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class OwnerDtoForOwner {

    private String FIO;

    private LocalDateTime dateBirthday;

    private String numberPassport;

    private String numberDriverLicense;

    private LocalDateTime dateExtradition;

    private String category;

    private List <AutoDtoForOwner> autoDtoForOwners;
}
