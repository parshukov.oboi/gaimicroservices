package com.gai.micro.mrogibddclient.service.impl;

import com.gai.micro.mrogibddclient.dto.forOwner.OwnerDtoForOwner;
import com.gai.micro.mrogibddclient.entity.OwnerEntity;
import com.gai.micro.mrogibddclient.mapper.AutoMapper;
import com.gai.micro.mrogibddclient.repository.OwnerRepository;
import com.gai.micro.mrogibddclient.service.OwnerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class OwnerServiceImpl implements OwnerService {

    @Autowired
    private OwnerRepository ownerRepository;

    @Autowired
    private AutoMapper autoMapper;

    @Override
    public OwnerDtoForOwner getOwner(String fio) {
        OwnerEntity ownerEntity = ownerRepository.getOwnerEntityByFIO(fio);
        return autoMapper.entityToDto(ownerEntity);
    }

}
