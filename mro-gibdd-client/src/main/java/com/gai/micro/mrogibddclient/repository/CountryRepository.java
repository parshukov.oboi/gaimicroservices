package com.gai.micro.mrogibddclient.repository;


import com.gai.micro.mrogibddclient.entity.CountryEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CountryRepository extends JpaRepository<CountryEntity,Long> {
}
