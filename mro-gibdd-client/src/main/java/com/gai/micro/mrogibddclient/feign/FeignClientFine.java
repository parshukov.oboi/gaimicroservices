package com.gai.micro.mrogibddclient.feign;

import com.gai.micro.mrogibddclient.dto.forCar.InsuranceDto;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@FeignClient(name = "feignFine",url = "localhost:8765")
public interface FeignClientFine {
    @GetMapping("/get-fine-for-mro")
    Boolean getFineInfo(@RequestParam String gosNumber);
}
