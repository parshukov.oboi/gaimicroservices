package com.gai.micro.mrogibddclient.dto.forCar;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class AMMarkDto {

    private String name;

    private FirmDto firmDto;

    private CountryDto countryDto;
}
