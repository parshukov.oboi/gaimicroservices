package com.gai.micro.mrogibddclient.entity;

import lombok.*;

import javax.persistence.*;
import java.sql.Date;
import java.time.LocalDateTime;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@Entity
@Table(name = "owner")
@NoArgsConstructor
@ToString
public class OwnerEntity {

    @Id
    @SequenceGenerator(name = "ownerSequence", sequenceName = "owner_sequence", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ownerSequence")
    @Column(name = "id")
    private Long id;

    @Column(name = "FIO")
    private String FIO;

    @Column(name = "date_birthday")
    private LocalDateTime dateBirthday;

    @Column(name = "number_passport")
    private String numberPassport;

    @Column(name = "number_driver_license")
    private String numberDriverLicense;

    @Column(name = "date_extradition")
    private LocalDateTime dateExtradition;

    @Column(name = "category")
    private String category;

    @OneToMany(
            fetch = FetchType.LAZY,
            cascade = CascadeType.ALL,
            mappedBy = "ownerEntity"
    )
    private List<AutoEntity> autoEntityList;
}
