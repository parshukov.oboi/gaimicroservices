package com.gai.micro.mrogibddclient.repository;

import com.gai.micro.mrogibddclient.entity.OwnerEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface OwnerRepository extends JpaRepository <OwnerEntity,Long> {

    OwnerEntity getOwnerEntityByFIO (String FIO);

}
