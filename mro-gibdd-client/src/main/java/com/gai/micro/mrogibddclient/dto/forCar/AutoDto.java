package com.gai.micro.mrogibddclient.dto.forCar;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.time.LocalDateTime;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class AutoDto {

    private String vinCode;

    private String gosNumber;

    private String NumberTechPassport;

    private LocalDateTime date;

    private String color;

    private AMMarkDto amMarkDto;

    private OwnerDto ownerDto;

}
