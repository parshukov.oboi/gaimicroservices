package com.gai.micro.mrogibddclient.dto.forOwner;

import com.gai.micro.mrogibddclient.dto.forCar.AMMarkDto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.time.LocalDateTime;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class AutoDtoForOwner {

    private String vinCode;

    private String gosNumber;

    private String NumberTechPassport;

    private LocalDateTime date;

    private String color;

    private AMMarkDto amMarkDto;
}
