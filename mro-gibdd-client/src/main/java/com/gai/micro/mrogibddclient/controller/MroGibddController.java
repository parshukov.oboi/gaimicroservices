package com.gai.micro.mrogibddclient.controller;

import com.gai.micro.mrogibddclient.dto.forCar.AutoDto;
import com.gai.micro.mrogibddclient.dto.forOwner.OwnerDtoForOwner;
import com.gai.micro.mrogibddclient.entity.AutoEntity;
import com.gai.micro.mrogibddclient.entity.FirmEntity;
import com.gai.micro.mrogibddclient.entity.OwnerEntity;
import com.gai.micro.mrogibddclient.feign.FeignClientFine;
import com.gai.micro.mrogibddclient.feign.FeignClientJacked;
import com.gai.micro.mrogibddclient.repository.FirmRepository;
import com.gai.micro.mrogibddclient.repository.OwnerRepository;
import com.gai.micro.mrogibddclient.service.AutoService;
import com.gai.micro.mrogibddclient.service.InsuranceService;
import com.gai.micro.mrogibddclient.service.OwnerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;

@RestController
public class MroGibddController {
    //убрать в сервисе
    @Autowired
    private OwnerService ownerService;

    @Autowired
    private InsuranceService insuranceService;

    @Autowired
    private AutoService autoService;

    @Autowired
    private FeignClientJacked feignClientJacked;

    @Autowired
    FeignClientFine feignClientFine;

    @Autowired
    OwnerRepository ownerRepository;

    @Autowired
    FirmRepository firmRepository;

    //Проверка на страховку
    @GetMapping("/get-insurance")
    public Boolean getInfo(@RequestParam String vinCode) {
        return insuranceService.getInsurance(vinCode);
    }

    //Проверка на угон
    @GetMapping("/get-jacked-car")
    public Boolean getJackedCar(@RequestParam String gosNumber){
        return feignClientJacked.getJackedInfo(gosNumber);
    }

    //проверка на штрафы
    @GetMapping("/get-fine")
    public Boolean getFine(@RequestParam String gosNumber){
        return feignClientFine.getFineInfo(gosNumber);
    }
    @GetMapping ("/get-auto")
    public AutoDto getAuto(@RequestParam String vinCode) {
        return autoService.getAutoDto(vinCode);
    }

    @GetMapping ("/get-owner")
    public OwnerDtoForOwner getOwner(@RequestParam String fio) {
        return ownerService.getOwner(fio);
    }

    @PostMapping("/add-owner")
    public void addOwner(@RequestBody OwnerEntity ownerEntity){ownerRepository.save(ownerEntity);}
    @PostMapping("/delete-auto")
    public void delAuto(@RequestParam String vinCode){
        autoService.delAuto(vinCode);
    }

    @PostMapping("/add-auto")
    public void addAuto(@RequestBody AutoEntity autoEntity){
        autoService.addAuto(autoEntity);
    }

//Добавить функционал (удаления владельца,добавление)
}
