package com.gai.micro.mrogibddclient.mapper;

import com.gai.micro.mrogibddclient.dto.forCar.*;
import com.gai.micro.mrogibddclient.dto.forOwner.AutoDtoForOwner;
import com.gai.micro.mrogibddclient.dto.forOwner.OwnerDtoForOwner;
import com.gai.micro.mrogibddclient.entity.AutoEntity;
import com.gai.micro.mrogibddclient.entity.OwnerEntity;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class AutoMapper {

    public AutoDto entityToDto (AutoEntity autoEntity){

        AutoDto autoDto = new AutoDto();
        AMMarkDto amMarkDto = new AMMarkDto();
        CountryDto countryDto = new CountryDto();
        FirmDto firmDto =new FirmDto();
        OwnerDto ownerDto = new OwnerDto();

        firmDto.setName(autoEntity.getAmMarkEntity().getFirmEntity().getName());
        countryDto.setName(autoEntity.getAmMarkEntity().getCountryEntity().getName());
        amMarkDto.setName(autoEntity.getAmMarkEntity().getName());
        amMarkDto.setCountryDto(countryDto);
        amMarkDto.setFirmDto(firmDto);

        ownerDto.setFIO(autoEntity.getOwnerEntity().getFIO());
        ownerDto.setCategory(autoEntity.getOwnerEntity().getCategory());
        ownerDto.setDateBirthday(autoEntity.getOwnerEntity().getDateBirthday());
        ownerDto.setDateExtradition(autoEntity.getOwnerEntity().getDateExtradition());
        ownerDto.setNumberDriverLicense(autoEntity.getOwnerEntity().getNumberDriverLicense());
        ownerDto.setNumberPassport(autoEntity.getOwnerEntity().getNumberPassport());


        autoDto.setVinCode(autoEntity.getVinCode());
        autoDto.setAmMarkDto(amMarkDto);
        autoDto.setDate(autoEntity.getDate());
        autoDto.setColor(autoEntity.getColor());
        autoDto.setGosNumber(autoEntity.getGosNumber());
        autoDto.setOwnerDto(ownerDto);
        autoDto.setNumberTechPassport(autoEntity.getNumberTechPassport());

        return autoDto;

    }

    public OwnerDtoForOwner entityToDto (OwnerEntity ownerEntity){

        OwnerDtoForOwner ownerDtoForOwner = new OwnerDtoForOwner();
        AMMarkDto amMarkDto = new AMMarkDto();
        CountryDto countryDto = new CountryDto();
        FirmDto firmDto = new FirmDto();
        List<AutoDtoForOwner> autoDtoForOwners = new ArrayList<>();

        for (int i = 0; i < ownerEntity.getAutoEntityList().size(); i++) {

            AutoDtoForOwner autoDto = new AutoDtoForOwner();
            firmDto.setName(ownerEntity.getAutoEntityList().get(i).getAmMarkEntity().getFirmEntity().getName());
            countryDto.setName(ownerEntity.getAutoEntityList().get(i).getAmMarkEntity().getCountryEntity().getName());
            amMarkDto.setName(ownerEntity.getAutoEntityList().get(i).getAmMarkEntity().getName());
            amMarkDto.setCountryDto(countryDto);
            amMarkDto.setFirmDto(firmDto);
            autoDto.setAmMarkDto(amMarkDto);

            autoDto.setVinCode(ownerEntity.getAutoEntityList().get(i).getVinCode());

            autoDto.setDate(ownerEntity.getAutoEntityList().get(i).getDate());
            autoDto.setColor(ownerEntity.getAutoEntityList().get(i).getColor());
            autoDto.setGosNumber(ownerEntity.getAutoEntityList().get(i).getGosNumber());
            autoDto.setNumberTechPassport(ownerEntity.getAutoEntityList().get(i).getNumberTechPassport());

            autoDtoForOwners.add(autoDto);

        }

        ownerDtoForOwner.setFIO(ownerEntity.getFIO());
        ownerDtoForOwner.setCategory(ownerEntity.getCategory());
        ownerDtoForOwner.setDateBirthday(ownerEntity.getDateBirthday());
        ownerDtoForOwner.setDateExtradition(ownerEntity.getDateExtradition());
        ownerDtoForOwner.setNumberDriverLicense(ownerEntity.getNumberDriverLicense());
        ownerDtoForOwner.setNumberPassport(ownerEntity.getNumberPassport());
        ownerDtoForOwner.setAutoDtoForOwners(autoDtoForOwners);

        return ownerDtoForOwner;
//        AutoDto autoDto = new AutoDto();
//        AMMarkDto amMarkDto = new AMMarkDto();
//        CountryDto countryDto = new CountryDto();
//        FirmDto firmDto =new FirmDto();
//        OwnerDto ownerDto = new OwnerDto();
//
//        firmDto.setName(ownerEntity..getAmMarkEntity().getFirmEntity().getName());
//        countryDto.setName(autoEntity.getAmMarkEntity().getCountryEntity().getName());
//        amMarkDto.setName(autoEntity.getAmMarkEntity().getName());
//        amMarkDto.setCountryDto(countryDto);
//        amMarkDto.setFirmDto(firmDto);
//
//        ownerDto.setFIO(autoEntity.getOwnerEntity().getFIO());
//        ownerDto.setCategory(autoEntity.getOwnerEntity().getCategory());
//        ownerDto.setDateBirthday(autoEntity.getOwnerEntity().getDateBirthday());
//        ownerDto.setDateExtradition(autoEntity.getOwnerEntity().getDateExtradition());
//        ownerDto.setNumberDriverLicense(autoEntity.getOwnerEntity().getNumberDriverLicense());
//        ownerDto.setNumberPassport(autoEntity.getOwnerEntity().getNumberPassport());
//
//
//        autoDto.setVinCode(autoEntity.getVinCode());
//        autoDto.setAmMarkDto(amMarkDto);
//        autoDto.setDate(autoEntity.getDate());
//        autoDto.setColor(autoEntity.getColor());
//        autoDto.setGosNumber(autoEntity.getGosNumber());
//        autoDto.setOwnerDto(ownerDto);
//        autoDto.setNumberTechPassport(autoEntity.getNumberTechPassport());
//
//        return autoDto;

    }

}
