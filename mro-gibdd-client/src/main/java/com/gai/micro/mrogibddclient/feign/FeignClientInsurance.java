package com.gai.micro.mrogibddclient.feign;

import com.gai.micro.mrogibddclient.dto.forCar.InsuranceDto;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@FeignClient(name = "feign",url = "localhost:8765")
public interface FeignClientInsurance {

    @GetMapping("/get-insurance-info")
    @ResponseBody InsuranceDto getInsuranceInfo(@RequestParam String vinCode);
}
