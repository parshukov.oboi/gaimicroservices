package com.gai.micro.mrogibddclient.service;

import com.gai.micro.mrogibddclient.dto.forCar.InsuranceDto;

public interface MroGibddService {

    public boolean dateCheck(InsuranceDto insuranceDto);
}
