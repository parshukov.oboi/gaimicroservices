package com.gai.micro.mrogibddclient.service;


import com.gai.micro.mrogibddclient.dto.forOwner.OwnerDtoForOwner;

public interface OwnerService {

    OwnerDtoForOwner getOwner (String fio);
}
