package com.gai.micro.mrogibddclient.service;

import com.gai.micro.mrogibddclient.dto.forCar.AutoDto;
import com.gai.micro.mrogibddclient.entity.AutoEntity;

public interface AutoService {

    AutoDto getAutoDto (String vin);

    void delAuto(String vincode);

    void addAuto(AutoEntity autoEntity);

}
