package com.gai.micro.mrogibddclient.entity;

import lombok.*;

import javax.persistence.*;
@Getter
@Setter
@AllArgsConstructor
@Entity
@Table(name = "firm")
@NoArgsConstructor
@ToString
public class FirmEntity {

    @Id
    @SequenceGenerator(name = "firmSequence", sequenceName = "firm_sequence", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "firmSequence")
    @Column(name = "id")
    private Long id;

    @Column(name = "name")
    private String name;
}
