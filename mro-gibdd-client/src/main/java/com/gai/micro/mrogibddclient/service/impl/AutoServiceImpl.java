package com.gai.micro.mrogibddclient.service.impl;

import com.gai.micro.mrogibddclient.dto.forCar.AutoDto;
import com.gai.micro.mrogibddclient.entity.AutoEntity;
import com.gai.micro.mrogibddclient.mapper.AutoMapper;
import com.gai.micro.mrogibddclient.repository.AutoRepository;
import com.gai.micro.mrogibddclient.service.AutoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AutoServiceImpl implements AutoService {

    @Autowired
    private AutoRepository autoRepository;
    @Autowired
    private AutoMapper autoMapper;

    @Override
    public AutoDto getAutoDto(String vin) {
        AutoEntity autoEntity = autoRepository.getAutoEntityByVinCode(vin);
        return  autoMapper.entityToDto(autoEntity);
    }

    @Override
    public void delAuto(String vinCode) {
        autoRepository.delete(autoRepository.getAutoEntityByVinCode(vinCode));
    }

    @Override
    public void addAuto(AutoEntity autoEntity) {
        if (autoRepository.getAutoEntityByVinCode(autoEntity.getVinCode())!=null){
            autoRepository.delete(autoRepository.getAutoEntityByVinCode(autoEntity.getVinCode()));
            autoRepository.save(autoEntity);
        }else{
            autoRepository.save(autoEntity);
        }
    }
}
