package com.gai.micro.mrogibddclient.entity;

import lombok.*;

import javax.persistence.*;
import java.sql.Date;
import java.time.LocalDateTime;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@Entity
@Table(name = "auto")
@NoArgsConstructor
@ToString
public class AutoEntity {

    @Id
    @SequenceGenerator(name = "autoSequence", sequenceName = "auto_sequence", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "autoSequence")
    @Column(name = "id")
    private Long id;

    @Column(name = "vin_code")
    private String vinCode;

    @Column(name = "gos_number")
    private String gosNumber;

    @Column(name = "number_tech_passport")
    private String NumberTechPassport;

    @Column(name = "data_registration")
    private LocalDateTime date;

    @Column(name = "color")
    private String color;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL,
            targetEntity = OwnerEntity.class)
    @JoinColumn(name = "id_owner")
    private OwnerEntity ownerEntity;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "id_code_mark")
    private AMMarkEntity amMarkEntity;
}
