package com.gai.micro.mrogibddclient.service;


public interface InsuranceService {
  Boolean getInsurance(String vin);
}
