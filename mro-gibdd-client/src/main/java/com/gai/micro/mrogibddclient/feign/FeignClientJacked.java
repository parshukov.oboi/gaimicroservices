package com.gai.micro.mrogibddclient.feign;

import com.gai.micro.mrogibddclient.dto.forCar.InsuranceDto;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@FeignClient(name = "feignJacked",url = "localhost:8765")
public interface FeignClientJacked {

    @GetMapping("/get-jacked")
    Boolean getJackedInfo(@RequestParam String gosNumber);
}
