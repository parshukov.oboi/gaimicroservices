package com.gai.micro.mrogibddclient.dto.forCar;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.time.LocalDateTime;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class InsuranceDto {

    private String vinCode;

    private LocalDateTime startDate;

    private LocalDateTime expirationDate;
}
