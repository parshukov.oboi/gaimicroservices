package com.gai.micro.mrogibddclient.entity;

import lombok.*;

import javax.persistence.*;

@Getter
@Setter
@AllArgsConstructor
@Entity
@Table(name = "AM_mark")
@NoArgsConstructor
@ToString
public class AMMarkEntity {

    @Id
    @SequenceGenerator(name = "markSequence", sequenceName = "mark_sequence", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "markSequence")
    @Column(name = "id")
    private Long id;

    @Column(name = "name")
    private String name;

    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL,optional = false)
    @JoinColumn(name = "id_firm")
    private FirmEntity firmEntity;

    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "id_country")
    private CountryEntity countryEntity;

}
