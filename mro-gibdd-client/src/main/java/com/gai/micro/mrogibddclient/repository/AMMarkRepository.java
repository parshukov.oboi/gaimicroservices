package com.gai.micro.mrogibddclient.repository;


import com.gai.micro.mrogibddclient.entity.AMMarkEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AMMarkRepository extends JpaRepository<AMMarkEntity,Long> {
}
