package com.gai.micro.mrogibddclient.dto.forCar;

import com.gai.micro.mrogibddclient.entity.AutoEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import java.time.LocalDateTime;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class OwnerDto {

    private String FIO;

    private LocalDateTime dateBirthday;

    private String numberPassport;

    private String numberDriverLicense;

    private LocalDateTime dateExtradition;

    private String category;

}
