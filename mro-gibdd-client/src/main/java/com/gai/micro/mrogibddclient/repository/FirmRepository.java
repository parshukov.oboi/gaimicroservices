package com.gai.micro.mrogibddclient.repository;

import com.gai.micro.mrogibddclient.entity.FirmEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FirmRepository extends JpaRepository<FirmEntity,Long> {
}
