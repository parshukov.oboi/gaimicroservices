package com.gai.micro.mrogibddclient.service.impl;

import com.gai.micro.mrogibddclient.dto.forCar.InsuranceDto;
import com.gai.micro.mrogibddclient.service.MroGibddService;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

@Service
public class MroGibddServiceImpl implements MroGibddService {

    @Override
    public boolean dateCheck(InsuranceDto insuranceDto) {
        LocalDateTime nowDate = LocalDateTime.now();
        if (nowDate.isAfter(insuranceDto.getExpirationDate())){
            return false;
        }else {
            return true;
        }
    }
}
