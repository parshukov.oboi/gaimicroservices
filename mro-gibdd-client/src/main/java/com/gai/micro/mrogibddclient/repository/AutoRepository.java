package com.gai.micro.mrogibddclient.repository;


import com.gai.micro.mrogibddclient.entity.AutoEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AutoRepository extends JpaRepository<AutoEntity,Long> {

    AutoEntity getAutoEntityByVinCode(String vinCode);
}
