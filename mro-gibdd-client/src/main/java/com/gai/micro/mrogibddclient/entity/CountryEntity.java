package com.gai.micro.mrogibddclient.entity;

import lombok.*;

import javax.persistence.*;

@Getter
@Setter
@AllArgsConstructor
@Entity
@Table(name = "country")
@NoArgsConstructor
@ToString
public class CountryEntity {

    @Id
    @SequenceGenerator(name = "countrySequence", sequenceName = "country_sequence", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "countrySequence")
    @Column(name = "id")
    private Long id;

    @Column(name = "name")
    private String name;
}
