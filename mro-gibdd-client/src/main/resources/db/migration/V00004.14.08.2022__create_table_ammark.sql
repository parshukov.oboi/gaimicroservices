create sequence mark_sequence start 1 increment 1;

CREATE TABLE IF NOT EXISTS AM_mark
(
    id  int8 PRIMARY KEY default nextval('mark_sequence'),
    name VARCHAR (100),
    id_firm INT8 NOT NULL REFERENCES country(id),
    id_country INT8 NOT NULL REFERENCES firm(id)
);

INSERT INTO AM_mark ( name,id_firm,id_country)
VALUES ( 'фольксваген поло седан',1,1),
       ( 'тойота рав 4',2,2),
       ( 'Шкода фабия',3,3);

