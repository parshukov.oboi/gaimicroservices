create sequence owner_sequence start 1 increment 1;

CREATE TABLE IF NOT EXISTS owner
(
    id  int8 PRIMARY KEY default nextval('owner_sequence'),
    FIO VARCHAR (100),
    date_birthday timestamp,
    number_passport VARCHAR (20),
    number_driver_license VARCHAR (20),
    date_extradition timestamp,
    category VARCHAR (100)
);

INSERT INTO owner (FIO, date_birthday,number_passport,number_driver_license,date_extradition,category)
VALUES ('Моисеев Евдоким Тарасович', '1994-04-30 00:00:00','6745 297704','5008189924','2027-06-30 00:00:00','B'),
       ('Пахомов Наум Егорович', '1995-08-30 00:00:00','4955 297664','2348189924','2021-06-30 00:00:00','B'),
       ('Зимин Парамон Арсеньевич', '1984-06-15 00:00:00','4345 256704','7808189924','2022-06-30 00:00:00','B'),
       ('Борисов Ярослав Богданович', '1996-01-23 00:00:00','3445 256704','7008679924','2023-06-30 00:00:00','C'),
       ('Юдин Гурий Денисович', '1993-06-30 00:00:00','4925 297704','2138189924','2024-06-30 00:00:00','B'),
       ('Селезнёв Степан Филиппович', '1994-06-30 00:00:00','4945 297704','5503489924','2025-06-30 00:00:00','A');

-- @Table(name = "owner")
-- @NoArgsConstructor
-- @ToString
-- public class OwnerEntity {
--
--     @Id
--     @SequenceGenerator(name = "ownerSequence", sequenceName = "owner_sequence", allocationSize = 1)
--     @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ownerSequence")
--     @Column(name = "id")
--     private Long id;
--
-- @Column(name = "FIO")
--     private String FIO;
--
-- @Column(name = "date_birthday")
--     private LocalDateTime dateBirthday;
--
-- @Column(name = "number_passport")
--     private String numberPassport;
--
-- @Column(name = "number_driver_license")
--     private String numberDriverLicense;
--
-- @Column(name = "date_extradition")
--     private LocalDateTime dateExtradition;
--
-- @Column(name = "category")
--     private String category;
-- }


