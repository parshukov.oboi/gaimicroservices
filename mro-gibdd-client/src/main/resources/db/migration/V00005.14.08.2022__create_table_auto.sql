create sequence auto_sequence start 1 increment 1;

CREATE TABLE IF NOT EXISTS auto
(
    id  int8 PRIMARY KEY default nextval('auto_sequence'),
    vin_code VARCHAR (20),
    id_owner INT8 REFERENCES owner(id),
    id_code_mark INT8 REFERENCES am_mark(id),
    gos_number VARCHAR (20),
    number_tech_passport VARCHAR (20),
    data_registration timestamp,
    color VARCHAR (100)
);

INSERT INTO auto (id_code_mark,id_owner, vin_code,gos_number,number_tech_passport,data_registration,color)
VALUES (2,1, 'WAUZZZ44ZEN096063','A120XM','78 KК 454667','2017-06-30 00:00:00','красный'),
       (1,2, 'WEEZZZ44ZEN092345','A230XM','67 ТК 898966','2011-06-30 00:00:00','ораньжевый'),
       (3,3, 'QWEZZZ44ZEN096789','A734XM','77 KК 345533','2012-06-30 00:00:00','желтый'),
       (3,1, 'WASDFZ44ZEN096543','D232XM','88 ТК 126556','2013-06-30 00:00:00','зеленый'),
       (2,3, 'WVBNMZ44ZEN093463','V678XM','80 KК 455667','2014-06-30 00:00:00','голубой'),
       (1,4, 'WQWEZZ44ZEN094363','B212XM','22 ТК 213323','2015-06-30 00:00:00','синий');



