create sequence firm_sequence start 1 increment 1;

CREATE TABLE IF NOT EXISTS firm
(
    id  int8 PRIMARY KEY default nextval('firm_sequence'),
    name VARCHAR (100)
);


INSERT INTO firm ( name)
VALUES ( 'Volkswagen'),
       ( 'Toyota'),
       ( 'Skoda');





