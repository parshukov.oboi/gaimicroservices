create sequence country_sequence start 1 increment 1;

CREATE TABLE IF NOT EXISTS country
(
    id  int8 PRIMARY KEY default nextval('country_sequence'),
    name VARCHAR (100)
);

INSERT INTO country (name)
VALUES ( 'Germany'),
       ( 'Japan'),
       ( 'Czech');






