GAImicroservices - это програма для управления БД в государственной авто инспекции, работающая на микросервисной архитектуре

<img alt="Diagram" src="db_gai.drawio.png"/>

Стек:
-Spring boot
-Spring cloud
-Spring data
-lombok
-mapstruct
-flyway

СУБД:  postgresql реализованный в docker контейнере

Методы:
1) добавление страховки, штрафов и угона
2) Проверка при регистрации на наличее штрафон, угона и страховки
3) Добавление и удаление автомобиля
4) Добавление и удаления владельца

Rest реализация:

<img alt="Diagram" src="eurekaService.drawio.png"/>
